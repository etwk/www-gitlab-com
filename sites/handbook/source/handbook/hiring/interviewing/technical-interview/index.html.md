---
layout: handbook-page-toc
title: "Technical Interview"
description: "Tips On How To Prepare For Your Technical Interview at GitLab."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Tips On How To Prepare For the Support Engineer Technical Interview

#### "Homework" before your interview

It is important to have knowledge of the GitLab product. We recommended getting some hands-on experience before your interview.  Spend time installing and using GitLab to get more familiar with the software and how to use it. Read through documentation and tutorials on using/configuring/troubleshooting GitLab.

- Read the [Intro to GitLab](https://docs.gitlab.com/ee/#new-to-git-and-gitlab)
- [Install GitLab](/install/) locally and explore the product
- Have a look at [common issues](https://docs.gitlab.com/omnibus/common_installation_problems/)
- Read through the online [forum](https://forum.gitlab.com/search?q=error) to see what errors stand out
- Read over some [useful commands](https://gist.github.com/dnozay/188f256839d4739ca3e4) when logging in locally to your GitLab installation

Review and practice:

- [Basic Linux Commands](https://www.dummies.com/computers/operating-systems/linux/common-linux-commands/)
- [Basic SSH Commands](https://www.ssh.com/ssh/command/)
- [PostgreSQL](https://docs.gitlab.com/ee/ci/services/postgres.html)
- [Git](https://docs.gitlab.com/ee/gitlab-basics/command-line-commands.html)

Familiarizing yourself with these will help with your communication through the hiring process, as well as prevent any roadblocks, and keep you focused on the tasks asked of you.

Other areas you may want to brush up on:    

- [Javascript](https://github.com/getify/You-Dont-Know-JS)
- [Chef](https://www.oreilly.com/library/view/learning-chef-for/9781491959442/)
- [Cloud-native Apps](https://www.redhat.com/en/topics/cloud-native-apps)

Remember to stay calm and do your best!  We appreciate that everyone gets the interview jitters and we are not here to be intimidating.  Practice makes perfect, so try and set aside some time to practice/study to be as best prepared as you can be.  If you get stuck along the way, try searching on the [forum](https://forum.gitlab.com/) to see if you are able to find a solution to your troubles!  Google can also be a great resource, which you are able to use even during your interview!  Knowing how to find the answer is just as important as the knowledge you already have.
